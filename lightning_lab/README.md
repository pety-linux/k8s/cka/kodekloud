# Solutions for Lightning Lab 1:

- **Task 1**
  <details>

  ```
  # According to docu:
  https://v1-20.docs.kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/

  # Debian commands for managing packages:
  apt-cache madison kubeadm
  apt-mark hold kubeadm
  apt-mark unhold kubeadm
  apt-mark showhold

  # Postup
  > controlplane
  apt-get update
  apt-mark unhold kubeadm
  apt-get install kubeadm=1.20.0-00

  kubeadm upgrade plan
  kubeadm upgrade apply v1.20.0
  ```

- **Task 2**
  <details>

  ```
  # Command
  kubectl get deploy -n admin2406 -o=custom-columns='DEPLOYMENT:.metadata.name,CONTAINER_IMAGE:.spec.template.   
  spec.containers[].image,READY_REPLICAS:.spec.replicas,NAMESPACE:.metadata.namespace' --sort-by=.metadata.name 
  > /opt/admin2406_data

  # Help
  kubectl get deploy -o json | jq -c 'path(..)'
  ```

- **Task 3**
  <details>

  ```
  # Test
  kubectl get no --kubeconfig=/root/CKA/admin.config

  # Fix issue
  correct port in provided kube config
  ```

- **Task 4**
  <details>
  
  ```
  kubectl create deployment nginx-deploy --image=nginx:1.16 --replicas=1

  kubectl set image deployment/nginx-deploy nginx=nginx:1.17 --record
  ```

- **Task 5**
  <details>
  
  ```
  apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    namespace: alpha
    name: mysql-alpha-pvc
  spec:
    storageClassName: slow
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: 1Gi
  ```

- **Task 6**
  <details>
  
  ```
  # etcdctl client install if not present
  wget "https://github.com/coreos/etcd/releases/download/v3.3.9/etcd-v3.3.9-linux-amd64.tar.gz"

  # snapshot
  export ETCTDCT_API=3

  etcdctl snapshot save --cert="/etc/kubernetes/pki/etcd/server.crt" --cacert="/etc/kubernetes/pki/etcd/ca.crt"  
  --key="/etc/kubernetes/pki/etcd/server.key" --endpoints="https://127.0.0.1:2379" /opt/etcd-backup.db

  Note: --endpoints not necessary if etcdctl run on the same machine where etcd listens to

  # status
  NOT recommended: etcdctl snapshot status /opt/etc-backup.db  -w="table"
  ```

- **Task 7**
  <details>
  
  ```
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      run: secret-1401
    name: secret-1401
    namespace: admin1401
  spec:
    containers:
    - args:
      - sleep
      - "4800"
      image: busybox
      name: secret-admin
      volumeMounts:
      - name: secret-volume
        mountPath: "/etc/secret-volume"
        readOnly: true
    volumes:
    - name: secret-volume
      secret:
        secretName: dotfile-secret
  ```
